#! /bin/bash
rpm --import https://packages.treasuredata.com/GPG-KEY-td-agent
cat >/etc/yum.repos.d/td.repo << 'EOF';
[treasuredata]
name=TreasureData
baseurl=http://packages.treasuredata.com/3/redhat/\$releasever/\$basearch
gpgcheck=1
gpgkey=https://packages.treasuredata.com/GPG-KEY-td-agent
EOF

yum check-update
yes | yum install -y td-agent

td-agent-gem install fluent-plugin-newrelic
td-agent-gem install fluent-plugin-grok-parser

mv /etc/td-agent/td-agent.conf /etc/td-agent/td-agent.conf.bkp
touch /etc/td-agent/td-agent.conf

mkdir -p /var/log/newrelic-infra
touch /var/log/newrelic-infra/test.log

cat > /etc/td-agent/td-agent.conf << 'EOF';
<source>
  @type tail
  @log_level warn
  <parse>
    @type grok
    <grok>
      pattern ^\[%{DATA:date}%{SPACE}%{TIME:time}\]%{SPACE}%{NOTSPACE:kind}%{SPACE}%{NOTSPACE:channel}%{SPACE}(?:Status: %{NOTSPACE:status} %{NOTSPACE:transaction}|Status %{NOTSPACE:status} %{NOTSPACE:transaction}|transaction:%{NOTSPACE:transaction}|event:%{NOTSPACE:event})%{SPACE}(?:transaction:%{NOTSPACE:transaction2}|pri:%{NOTSPACE:pri}|message:%{NOTSPACE:message}|)
    </grok>
  </parse>
  path /var/log/newrelic-infra/test.log
  pos_file /tmp/td-agent/log_file.pos
  tag *
</source>

<match **>
  @type newrelic
  api_key $NEW_RELIC_API_KEY
  base_uri https://log-api.newrelic.com/log/v1
  <buffer>
    @type memory
    flush_interval 5s
  </buffer>
</match>
EOF
