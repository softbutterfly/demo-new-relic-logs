# DEMO NEW RELIC LOGS: Fluentd en CentOS 6.10

## Procedimiento

1. Instalación de fluentd

```bash
rpm --import https://packages.treasuredata.com/GPG-KEY-td-agent
cat >/etc/yum.repos.d/td.repo << 'EOF';
[treasuredata]
name=TreasureData
baseurl=http://packages.treasuredata.com/3/redhat/\$releasever/\$basearch
gpgcheck=1
gpgkey=https://packages.treasuredata.com/GPG-KEY-td-agent
EOF

yum check-update
yes | yum install -y td-agent
```

2. Instalación de plugins de fluentd necesarios

```bash
td-agent-gem install fluent-plugin-newrelic
td-agent-gem install fluent-plugin-grok-parser
```

3. Copia de respaldo de la configuración por defecto

```bash
mv /etc/td-agent/td-agent.conf /etc/td-agent/td-agent.conf.bkp
```

4. Escribir la nueva configuración

```bash
touch /etc/td-agent/td-agent.conf

cat > /etc/td-agent/td-agent.conf << 'EOF';
<source>
  @type tail
  @log_level warn
  <parse>
    @type grok
    <grok>
      pattern <PUT YOUR GROK PATTERN>
    </grok>
  </parse>
  path /path/to/your/logs/file.log
  pos_file /tmp/td-agent/unique_name_file.pos
  tag your_sample_tag
</source>

<match **>
  @type newrelic
  api_key $NEW_RELIC_API_KEY
  base_uri https://log-api.newrelic.com/log/v1
  <buffer>
    @type memory
    flush_interval 5s
  </buffer>
</match>
EOF
```

5. Reiniciar el servicio

```bash
/etc/init.d/td-agent stop
/etc/init.d/td-agent start
```

## Resolucion de problemas

1. ¿Cómo usar con proxy?

Abrir el archivo `/etc/init.d/td-agent` y a continuación de la linea que inicia con `export PATH` colocar lo siguiente.

```bash
export http_proxy=http://user:password@host:port
```

2. ¿Problemas para inciar, detener y reiniciar el servicio?

Asegurese de que los permisos de las carpetas, y archivos de td-agent sean los correctos

|Key                  | Value         |
|---------------------|---------------|
|Owner                | td-agent      |
|Group                | td-agent      |
|File Permissions     | 644           |
|Directory Permissions| 755           |

3. Para Latinia emplear el siguiente Grok regex

```
^\[%{DATA:date}%{SPACE}%{TIME:time}\]%{SPACE}%{NOTSPACE:kind}%{SPACE}%{NOTSPACE:channel}%{SPACE}(?:Status: %{NOTSPACE:status} %{NOTSPACE:transaction}|Status %{NOTSPACE:status} %{NOTSPACE:transaction}|transaction:%{NOTSPACE:transaction}|event:%{NOTSPACE:event})%{SPACE}(?:transaction:%{NOTSPACE:transaction2}|pri:%{NOTSPACE:pri}|message:%{NOTSPACE:message}|)
```
