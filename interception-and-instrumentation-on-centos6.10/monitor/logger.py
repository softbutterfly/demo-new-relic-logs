import re
import sys
import time
import random
from datetime import datetime


def fake_log_generator(source, fakelog, use_ms):
    fakelog_handler = open(fakelog, 'w')

    timestamp_pattern = '[%Y/%m/%d %H:%M:%S]'

    if use_ms:
        timestamp_pattern = '[%Y/%m/%d %H:%M:%S.%f]'

    with open(source, 'r') as source_filehandler:
        for line in source_filehandler.readlines():
            line = line.strip()

            if not line:
                break

            splitted_line = line.split(" ", 2)
            timestamp = datetime.now().strftime(timestamp_pattern)
            splitted_line[0:2] = timestamp.split(" ")
            line = " ".join(splitted_line) + "\n"

            fakelog_handler.write(line)
            time.sleep(random.random() / 100)



if __name__ == '__main__':
    source = sys.argv[1]
    fakelog = sys.argv[2]
    use_ms = bool(sys.argv[3]) if len(sys.argv) > 3 else False
    fake_log_generator(source, fakelog, use_ms)
